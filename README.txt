=========================
AUDIO FOLDER PLAYER DRUPAL MODULE
=========================
by Freakk
ffr3akk@gmail.com
http://www.freakk.net
http://drupal.org/user/2762953

Maintainers:
  Freakk (http://drupal.org/user/2762953 , http://www.freakk.net)


CONTENTS OF THIS FILE
----------------------

  * Introduction
  * Features
  * Installation
  * Known Issues
  * To do  


INTRODUCTION
------------
Audio Folder Player is an HTML5 audio player module with automatic playlist generation, obtained by dynamically scanning a user-specified folder containing the mp3 files.


FEATURES
------------
- Automatic playlist from folder
- Playlist panel
- CSS3 user interface (no image files used)
- Autoplay


INSTALLATION
------------
1. Copy audio_folder_player folder to modules directory.
2. At admin/build/modules enable the audio_folder_player module.
4. Configure the module at admin/config/content/audio_folder_player.


KNOWN ISSUES
------------
No known issues reported yet.

TO DO
-----
1. Shuffle
